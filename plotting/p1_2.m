clc
clear all
close all

A= load('data/1/2/random.txt');
B= load('data/1/2/greedy0.0.txt');
C= load('data/1/2/greedy0.1.txt');
D= load('data/1/2/greedy0.2.txt');
E= load('data/1/2/softmax1.0.txt');
F= load('data/1/2/softmax0.1.txt');

figure
hold on
plot(A(:,1),'b');
plot(B(:,1),'r');
plot(C(:,1),'g');
plot(D(:,1),'y');
plot(E(:,1),'m');
plot(F(:,1),'c');
plot(1:1000,2.3,'k');
legend('Random','Greedy-0','Greedy-0.1','Greedy-0.2','Softmax1','softmax0.1','Q*ai');
axis([0 1000 0 4]);
title('Expected value - Arm 1');
xlabel('Iterations');
ylabel('Value');
print('eps/1/2_1','-depsc');

figure
hold on
plot(A(:,2),'b');
plot(B(:,2),'r');
plot(C(:,2),'g');
plot(D(:,2),'y');
plot(E(:,2),'m');
plot(F(:,2),'c');
plot(1:1000,2.1,'k');
legend('Random','Greedy-0','Greedy-0.1','Greedy-0.2','Softmax1','softmax0.1','Q*ai');
axis([0 1000 0 4]);
title('Expected value - Arm 2');
xlabel('Iterations');
ylabel('Value');
print('eps/1/2_2','-depsc');

figure
hold on
plot(A(:,3),'b');
plot(B(:,3),'r');
plot(C(:,3),'g');
plot(D(:,3),'y');
plot(E(:,3),'m');
plot(F(:,3),'c');
plot(1:1000,1.5,'k');
legend('Random','Greedy-0','Greedy-0.1','Greedy-0.2','Softmax1','softmax0.1','Q*ai');
axis([0 1000 0 4]);
title('Expected value - Arm 3');
xlabel('Iterations');
ylabel('Value');
print('eps/1/2_3','-depsc');

figure
hold on
plot(A(:,4),'b');
plot(B(:,4),'r');
plot(C(:,4),'g');
plot(D(:,4),'y');
plot(E(:,4),'m');
plot(F(:,4),'c');
plot(1:1000,1.3,'k');
legend('Random','Greedy-0','Greedy-0.1','Greedy-0.2','Softmax1','softmax0.1','Q*ai');
axis([0 1000 0 2]);
title('Expected value - Arm 4');
xlabel('Iterations');
ylabel('Value');
print('eps/1/2_4','-depsc');
