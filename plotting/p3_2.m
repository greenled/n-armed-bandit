clc
clear all
close all

A= load('data/3/2/random.txt');
B= load('data/3/2/greedy0.0.txt');
C= load('data/3/2/greedy0.1.txt');
D= load('data/3/2/greedy0.2.txt');
E= load('data/3/2/softmax1.0.txt');
F= load('data/3/2/softmax0.1.txt');
G= load('data/3/2/timeAwareGreedy.txt');
H= load('data/3/2/timeAwareSoftmax.txt');

figure
hold on
plot(A(:,1));
plot(B(:,1));
plot(C(:,1));
plot(D(:,1));
plot(E(:,1));
plot(F(:,1));
plot(G(:,1),'Color',[0.9412 0.4706 0]);
plot(H(:,1),'Color',[0.502 0.251 0]);
plot(1:1000,2.3,'k');
legend('Random','Greedy-0','Greedy-0.1','Greedy-0.2','Softmax1','softmax0.1','Time-aware Greedy','Time-aware Softmax','Q*ai');
axis([0 1000 0 4]);
title('Expected value - Arm 1');
xlabel('Iterations');
ylabel('Value');
print('eps/3/2_1','-depsc');

figure
hold on
plot(A(:,2));
plot(B(:,2));
plot(C(:,2));
plot(D(:,2));
plot(E(:,2));
plot(F(:,2));
plot(G(:,2),'Color',[0.9412 0.4706 0]);
plot(H(:,2),'Color',[0.502 0.251 0]);
plot(1:1000,2.1,'k');
legend('Random','Greedy-0','Greedy-0.1','Greedy-0.2','Softmax1','softmax0.1','Time-aware Greedy','Time-aware Softmax','Q*ai');
axis([0 1000 0 4]);
title('Expected value - Arm 2');
xlabel('Iterations');
ylabel('Value');
print('eps/3/2_2','-depsc');

figure
hold on
plot(A(:,3));
plot(B(:,3));
plot(C(:,3));
plot(D(:,3));
plot(E(:,3));
plot(F(:,3));
plot(G(:,3),'Color',[0.9412 0.4706 0]);
plot(H(:,3),'Color',[0.502 0.251 0]);
plot(1:1000,1.5,'k');
legend('Random','Greedy-0','Greedy-0.1','Greedy-0.2','Softmax1','softmax0.1','Time-aware Greedy','Time-aware Softmax','Q*ai');
axis([0 1000 0 4]);
title('Expected value - Arm 3');
xlabel('Iterations');
ylabel('Value');
print('eps/3/2_3','-depsc');

figure
hold on
plot(A(:,4));
plot(B(:,4));
plot(C(:,4));
plot(D(:,4));
plot(E(:,4));
plot(F(:,4));
plot(G(:,4),'Color',[0.9412 0.4706 0]);
plot(H(:,4),'Color',[0.502 0.251 0]);
plot(1:1000,1.3,'k');
legend('Random','Greedy-0','Greedy-0.1','Greedy-0.2','Softmax1','softmax0.1','Time-aware Greedy','Time-aware Softmax','Q*ai');
axis([0 1000 0 4]);
title('Expected value - Arm 4');
xlabel('Iterations');
ylabel('Value');
print('eps/3/2_4','-depsc');
