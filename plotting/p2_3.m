clc
clear all
close all

A= load('data/2/3/random.txt');
B= load('data/2/3/greedy0.0.txt');
C= load('data/2/3/greedy0.1.txt');
D= load('data/2/3/greedy0.2.txt');
E= load('data/2/3/softmax1.0.txt');
F= load('data/2/3/softmax0.1.txt');

figure
hold all
hist(A);
set(gca,'XTick',0:3);
set(gca,'XTickLabel',{'1','2','3','4'});
title('Histogram - Random');
xlabel('Arms');
ylabel('K-value');
print('eps/2/3_1','-depsc');

figure
hist(B);
set(gca,'XTick',0:3);
set(gca,'XTickLabel',{'1','2','3','4'});
title('Histogram - Greedy0.0');
xlabel('Arms');
ylabel('K-value');
print('eps/2/3_2','-depsc');

figure
hist(C);
set(gca,'XTick',0:3);
set(gca,'XTickLabel',{'1','2','3','4'});
title('Histogram - Greedy0.1');
xlabel('Arms');
ylabel('K-value');
print('eps/2/3_3','-depsc');

figure
hist(D);
set(gca,'XTick',0:3);
set(gca,'XTickLabel',{'1','2','3','4'});
title('Histogram - Greedy0.2');
xlabel('Arms');
ylabel('K-value');
print('eps/2/3_4','-depsc');

figure
hist(E);
set(gca,'XTick',0:3);
set(gca,'XTickLabel',{'1','2','3','4'});
title('Histogram - Softmax1.0');
xlabel('Arms');
ylabel('K-value');
print('eps/2/3_5','-depsc');

figure
hist(F);
set(gca,'XTick',0:3);
set(gca,'XTickLabel',{'1','2','3','4'});
title('Histogram - Softmax0.1');
xlabel('Arms');
ylabel('K-value');
print('eps/2/3_6','-depsc');
