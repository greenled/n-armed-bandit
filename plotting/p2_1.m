clc
clear all
close all

A= load('data/2/1/random.txt');
B= load('data/2/1/greedy0.0.txt');
C= load('data/2/1/greedy0.1.txt');
D= load('data/2/1/greedy0.2.txt');
E= load('data/2/1/softmax1.0.txt');
F= load('data/2/1/softmax0.1.txt');

figure
hold on
plot(A,'b');
plot(B,'r');
plot(C,'g');
plot(D,'y');
plot(E,'k');
plot(F,'m');
legend('Random','Greedy-0','Greedy-0.1','Greedy-0.2','Softmax1','Softmax0.1');
axis([0 1000 -2.5 5.5]);
title('N-BANDITS Average Rewards');
xlabel('Iterations');
ylabel('Average');
print('eps/2/1','-depsc');
