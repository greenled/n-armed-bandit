clc
clear all
close all

A= load('data/3/1/random.txt');
B= load('data/3/1/greedy0.0.txt');
C= load('data/3/1/greedy0.1.txt');
D= load('data/3/1/greedy0.2.txt');
E= load('data/3/1/softmax1.0.txt');
F= load('data/3/1/softmax0.1.txt');
G= load('data/3/1/timeAwareGreedy.txt');
H= load('data/3/1/timeAwareSoftmax.txt');

figure
hold on
plot(A,'b');
plot(B,'r');
plot(C,'g');
plot(D,'y');
plot(E,'k');
plot(F,'m');
plot(G,'Color',[0.9412 0.4706 0]);
plot(H,'Color',[0.502 0.251 0]);
legend('Random','Greedy-0','Greedy-0.1','Greedy-0.2','Softmax1','Softmax0.1','Time-aware Greedy','Time-aware Softmax');
axis([0 1000 -0.5 3.5]);
title('N-BANDITS Average Rewards');
xlabel('Iterations');
ylabel('Average');
print('eps/3/1','-depsc');
