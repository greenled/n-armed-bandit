package cu.uclv.vlir;

class Arm {
    private final double standardDeviation;
    private final double mean;

    public Arm(double mean, double standardDeviation) {
        this.mean = mean;
        this.standardDeviation = standardDeviation;
    }

    public double play () {
        return Util.calculateRandomWithNormalDistribution (mean, standardDeviation);
    }
}
