package cu.uclv.vlir;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

class Util {
    /**
     * Generate a number using the Normal distribution
     * @param mean
     * @param standardDeviation
     * @return generated number
     */
    public static double calculateRandomWithNormalDistribution(double mean, double standardDeviation) {
        return new Random().nextDouble() * standardDeviation + mean;
    }

    /**
     * Remove surounding brackets from stringified array
     * @param stringifiedArray
     * @return stringified array without surrounding brackets
     */
    public static String removeBrackets (String stringifiedArray) {
        String str1 = stringifiedArray.substring(1, stringifiedArray.length());
        return str1.substring(0, str1.length() - 1);
    }

    /**
     * Q selection strategy - random
     * @param armsNumber
     * @return qIndex
     */
    public static int selectRandomQ(int armsNumber) {
        return (int) Math.floor(Math.random() * (armsNumber));
    }

    /**
     * Q selection strategy - e-greedy
     * @param qValues
     * @param armsNumber
     * @param epsilon
     * @return qIndex
     */
    public static int selectEpsilonGreedy(double[] qValues, int armsNumber, double epsilon) {
        double randomValue = Math.random();
        if(randomValue < epsilon){
            return selectRandomQ(armsNumber);
        }else{
            return selectGreedy(qValues);
        }
    }

    /**
     * Q selection strategy - greedy
     * @param qValues
     * @return qIndex
     */
    private static int selectGreedy(double[] qValues){
        double qValue = qValues[0];
        int selectedIndex = 0;
        for(int i = 1; i < qValues.length; i++){
            if(qValues[i] > qValue) {
                qValue = qValues[i];
                selectedIndex = i;
            }
        }
        return selectedIndex;
    }

    /**
     * Q selection strategy - softmap
     * @param qValues
     * @param tao
     * @return qIndex
     */
    public static int selectSoftmax(double[] qValues, double tao) {
        double qValuesSum = 0;
        for (Double qValue : qValues) {
            qValuesSum += Math.exp(qValue / tao);
        }

        List<Double> probabilities = new ArrayList<>();
        for (Double Qa : qValues) {
            probabilities.add(Math.exp(Qa / tao) / qValuesSum);
        }

        int qValueWithHighestProbability = 0;
        double minProb = 0;
        double maxProb = 0;
        double qProb = Math.random();
        for (int i = 0; i < probabilities.size(); i++) {
            double probability = probabilities.get(i);
            if (probability != 0) {
                maxProb += probability;
                if ((maxProb >= qProb) && (qProb > minProb)) {
                    qValueWithHighestProbability = i;
                    break;
                }
                minProb = maxProb;
            }
        }

        return qValueWithHighestProbability;
    }

    /**
     * Write text to external file in a data directory
     * @param content
     * @param path
     */
    public static void writeToDataOutput(String content, Path path) {
        try {
            Files.write(path, content.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create data directories
     */
    public static void resetDataDirs (String dataOutputDir) {
        Path dataOutputDirectory = Paths.get(dataOutputDir);

        List<String> workspaces = Arrays.asList("1", "2", "3");
        workspaces.forEach(s -> {
            Path workspace = dataOutputDirectory.resolve(s);
            Path report1 = workspace.resolve("1");
            Path report2 = workspace.resolve("2");
            Path report3 = workspace.resolve("3");
            Path reportTime = workspace.resolve("time");

            try {
                Files.createDirectory(workspace);
                Files.createDirectory(report1);
                Files.createDirectory(report2);
                Files.createDirectory(report3);
                Files.createDirectory(reportTime);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public static String arrayToLines (double[] array) {
        String lines = "";
        for (double a : array) {
            lines += a + "\n";
        }
        return lines;
    }

    public static String arrayToLines (int[] array) {
        String lines = "";
        for (int a : array) {
            lines += a + "\n";
        }
        return lines;
    }

    public static String multiArrayToLines (double[][] array) {
        String lines = "";
        for (double[] a : array) {
            /*String q = "";
            for (double d : a) {
                q += d+",";
            }
            System.out.println(q);*/

            String line = "";
            for (int i = 0; i < a.length; i++) {
                line += a[i]+"";
                if (i < a.length - 1) {
                    line += ",";
                }
            }
            lines += line + "\n";
        }
        return lines;
    }
}
