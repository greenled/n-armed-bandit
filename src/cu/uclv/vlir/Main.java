package cu.uclv.vlir;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        String dataOutputDir = args[0];

        Util.resetDataDirs(dataOutputDir);

        int iterations = 1000;

        Path dataOutputDirectory = Paths.get(dataOutputDir);

        List<Arm> arms1 = Arrays.asList(
                new Arm(2.3, 0.6),
                new Arm(2.1, 0.9),
                new Arm(1.5, 2),
                new Arm(1.3, 0.4)
        );

        System.out.println("---1---");
        Path workspace1 = dataOutputDirectory.resolve("1");
        simulateRandom(arms1, iterations, workspace1);
        simulateGreedy(arms1, iterations, 0, workspace1);
        simulateGreedy(arms1, iterations, 0.1, workspace1);
        simulateGreedy(arms1, iterations, 0.2, workspace1);
        simulateSoftmax(arms1, iterations, 1, workspace1);
        simulateSoftmax(arms1, iterations, 0.1, workspace1);

        List<Arm> arms2 = Arrays.asList(
                new Arm(2.3, 1.2),
                new Arm(2.1, 1.8),
                new Arm(1.5, 4),
                new Arm(1.3, 0.8)
        );

        System.out.println("---2---");
        Path workspace2 = dataOutputDirectory.resolve("2");
        simulateRandom(arms2, iterations, workspace2);
        simulateGreedy(arms2, iterations, 0, workspace2);
        simulateGreedy(arms2, iterations, 0.1, workspace2);
        simulateGreedy(arms2, iterations, 0.2, workspace2);
        simulateSoftmax(arms2, iterations, 1, workspace2);
        simulateSoftmax(arms2, iterations, 0.1, workspace2);

        List<Arm> arms3 = Arrays.asList(
                new Arm(2.3, 0.6),
                new Arm(2.1, 0.9),
                new Arm(1.5, 2),
                new Arm(1.3, 0.4)
        );

        System.out.println("---3---");
        Path workspace3 = dataOutputDirectory.resolve("3");
        simulateRandom(arms3, iterations, workspace3);
        simulateGreedy(arms3, iterations, 0, workspace3);
        simulateGreedy(arms3, iterations, 0.1, workspace3);
        simulateGreedy(arms3, iterations, 0.2, workspace3);
        simulateSoftmax(arms3, iterations, 1, workspace3);
        simulateSoftmax(arms3, iterations, 0.1, workspace3);
        simulateTimeAwareGreedy(arms3, iterations, workspace3);
        simulateTimeAwareSoftmax(arms3, iterations, workspace3);
    }

    private static void simulateRandom(List<Arm> arms, int iterations, Path workspace) {
        System.out.println("Simulating Random");
        long startTime = System.currentTimeMillis();
        double[][] qValues = new double[iterations][arms.size()];
        int[] kValues = new int[arms.size()];

        int[] histogram = new int[iterations];
        double[] averageRValues = new double[iterations];
        double cumulativeReward = 0;

        for (int i = 0; i < iterations; i++) {
            if (i == 0) {
                qValues[i] = new double[arms.size()];
            } else {
                qValues[i] = Arrays.copyOf(qValues[i-1], arms.size());
            }

            int qIndex = Util.selectRandomQ(arms.size());
            double reward = arms.get(qIndex).play();
            cumulativeReward += reward;
            qValues[i][qIndex] = qValues[i][qIndex] + (1.0 / (kValues[qIndex] + 1.0)) * (reward - qValues[i][qIndex]);
            kValues[qIndex]++;
            histogram[i] = qIndex;

            if (i == 0) {
                averageRValues[i] = cumulativeReward;
            } else {
                averageRValues[i] = cumulativeReward / (i + 1);
            }
        }

        Util.writeToDataOutput((System.currentTimeMillis() - startTime)+"", workspace.resolve("time/random.txt"));
        Util.writeToDataOutput(Util.arrayToLines(averageRValues), workspace.resolve("1/random.txt"));
        Util.writeToDataOutput(Util.multiArrayToLines(qValues), workspace.resolve("2/random.txt"));
        Util.writeToDataOutput(Util.arrayToLines(histogram), workspace.resolve("3/random.txt"));
    }

    private static void simulateGreedy(List<Arm> arms, int iterations, double epsilon, Path workspace) {
        System.out.println("Simulating Greedy (epsilon="+epsilon+")");
        long startTime = System.currentTimeMillis();
        double[][] qValues = new double[iterations][arms.size()];
        int[] kValues = new int[arms.size()];

        int[] histogram= new int[iterations];
        double[] averageRValues = new double[iterations];
        double cumulativeReward = 0;

        for (int i = 0; i < iterations; i++) {
            if (i == 0) {
                qValues[i] = new double[arms.size()];
            } else {
                qValues[i] = Arrays.copyOf(qValues[i-1], arms.size());
            }

            int qIndex = Util.selectEpsilonGreedy(qValues[i], arms.size(), epsilon);
            double reward = arms.get(qIndex).play();
            cumulativeReward += reward;
            qValues[i][qIndex] = qValues[i][qIndex] + (1.0 / (kValues[qIndex] + 1.0)) * (reward - qValues[i][qIndex]);
            kValues[qIndex]++;
            histogram[i] = qIndex;

            if (i == 0) {
                averageRValues[i] = cumulativeReward;
            } else {
                averageRValues[i] = cumulativeReward / (i + 1);
            }
        }

        Util.writeToDataOutput((System.currentTimeMillis() - startTime)+"", workspace.resolve("time/greedy"+epsilon+".txt"));
        Util.writeToDataOutput(Util.arrayToLines(averageRValues), workspace.resolve("1/greedy"+epsilon+".txt"));
        Util.writeToDataOutput(Util.multiArrayToLines(qValues), workspace.resolve("2/greedy"+epsilon+".txt"));
        Util.writeToDataOutput(Util.arrayToLines(histogram), workspace.resolve("3/greedy"+epsilon+".txt"));
    }

    private static void simulateTimeAwareGreedy(List<Arm> arms, int iterations, Path workspace) {
        System.out.println("Simulating time aware Greedy");
        long startTime = System.currentTimeMillis();
        double[][] qValues = new double[iterations][arms.size()];
        int[] kValues = new int[arms.size()];

        int[] histogram= new int[iterations];
        double[] averageRValues = new double[iterations];
        double cumulativeReward = 0;

        for (int i = 0; i < iterations; i++) {
            if (i == 0) {
                qValues[i] = new double[arms.size()];
            } else {
                qValues[i] = Arrays.copyOf(qValues[i-1], arms.size());
            }

            double epsilon = 1 / Math.sqrt(i+1);
            int qIndex = Util.selectEpsilonGreedy(qValues[i], arms.size(), epsilon);
            double reward = arms.get(qIndex).play();
            cumulativeReward += reward;
            qValues[i][qIndex] = qValues[i][qIndex] + (1.0 / (kValues[qIndex] + 1.0)) * (reward - qValues[i][qIndex]);
            kValues[qIndex]++;
            histogram[i] = qIndex;

            if (i == 0) {
                averageRValues[i] = cumulativeReward;
            } else {
                averageRValues[i] = cumulativeReward / (i + 1);
            }
        }

        Util.writeToDataOutput((System.currentTimeMillis() - startTime)+"", workspace.resolve("time/timeAwareGreedy.txt"));
        Util.writeToDataOutput(Util.arrayToLines(averageRValues), workspace.resolve("1/timeAwareGreedy.txt"));
        Util.writeToDataOutput(Util.multiArrayToLines(qValues), workspace.resolve("2/timeAwareGreedy.txt"));
        Util.writeToDataOutput(Util.arrayToLines(histogram), workspace.resolve("3/timeAwareGreedy.txt"));
    }

    private static void simulateSoftmax(List<Arm> arms, int iterations, double tao, Path workspace) {
        System.out.println("Simulating Softmax (tao="+tao+")");
        long startTime = System.currentTimeMillis();
        double[][] qValues = new double[iterations][arms.size()];
        int[] kValues = new int[arms.size()];

        int[] histogram = new int[iterations];
        double[] averageRValues = new double[iterations];
        double cumulativeReward = 0;

        for (int i = 0; i < iterations; i++) {
            if (i == 0) {
                qValues[i] = new double[arms.size()];
            } else {
                qValues[i] = Arrays.copyOf(qValues[i-1], arms.size());
            }

            int qIndex = Util.selectSoftmax(qValues[i], tao);
            double reward = arms.get(qIndex).play();
            cumulativeReward += reward;
            qValues[i][qIndex] = qValues[i][qIndex] + (1.0 / (kValues[qIndex] + 1.0)) * (reward - qValues[i][qIndex]);
            kValues[qIndex]++;
            histogram[i] = qIndex;

            if (i == 0) {
                averageRValues[i] = cumulativeReward;
            } else {
                averageRValues[i] = cumulativeReward / (i + 1);
            }
        }

        Util.writeToDataOutput((System.currentTimeMillis() - startTime)+"", workspace.resolve("time/softmax"+tao+".txt"));
        Util.writeToDataOutput(Util.arrayToLines(averageRValues), workspace.resolve("1/softmax"+tao+".txt"));
        Util.writeToDataOutput(Util.multiArrayToLines(qValues), workspace.resolve("2/softmax"+tao+".txt"));
        Util.writeToDataOutput(Util.arrayToLines(histogram), workspace.resolve("3/softmax"+tao+".txt"));
    }

    private static void simulateTimeAwareSoftmax(List<Arm> arms, int iterations, Path workspace) {
        System.out.println("Simulating time aware Softmax");
        long startTime = System.currentTimeMillis();
        double[][] qValues = new double[iterations][arms.size()];
        int[] kValues = new int[arms.size()];

        int[] histogram= new int[iterations];
        double[] averageRValues = new double[iterations];
        double cumulativeReward = 0;

        for (int i = 0; i < iterations; i++) {
            if (i == 0) {
                qValues[i] = new double[arms.size()];
            } else {
                qValues[i] = Arrays.copyOf(qValues[i-1], arms.size());
            }

            double tao = 4 * (iterations - (i + 1)) / iterations ;
            int qIndex = Util.selectSoftmax(qValues[i], tao);
            double reward = arms.get(qIndex).play();
            cumulativeReward += reward;
            qValues[i][qIndex] = qValues[i][qIndex] + (1.0 / (kValues[qIndex] + 1.0)) * (reward - qValues[i][qIndex]);
            kValues[qIndex]++;
            histogram[i] = qIndex;

            if (i == 0) {
                averageRValues[i] = cumulativeReward;
            } else {
                averageRValues[i] = cumulativeReward / (i + 1);
            }
        }

        Util.writeToDataOutput((System.currentTimeMillis() - startTime)+"", workspace.resolve("time/timeAwareSoftmax.txt"));
        Util.writeToDataOutput(Util.arrayToLines(averageRValues), workspace.resolve("1/timeAwareSoftmax.txt"));
        Util.writeToDataOutput(Util.multiArrayToLines(qValues), workspace.resolve("2/timeAwareSoftmax.txt"));
        Util.writeToDataOutput(Util.arrayToLines(histogram), workspace.resolve("3/timeAwareSoftmax.txt"));
    }

}
